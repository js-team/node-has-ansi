# Installation
> `npm install --save @types/has-ansi`

# Summary
This package contains type definitions for has-ansi (https://github.com/chalk/has-ansi#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/has-ansi.

### Additional Details
 * Last updated: Mon, 26 Apr 2021 21:31:24 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [BendingBender](https://github.com/BendingBender).
